<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Technics extends Model
{
    public function getTechType(){
        return DB::select('select distinct(product_type) from tech.products order by product_type');
    }

    public function getTech($product_type)
    {
        $query = DB::table('products');
        $query->select('name', 'id_product', 'price', 'product_type')
            ->orderBy('name');
        if ($product_type) {
            $query->where('product_type', '=', $product_type);
        }
        $tech = $query->get();
        return $tech;
    }


    public function getTechByID($id)
    {
        if(!$id) return null;
        $tech = DB::table('products')
            ->select('*')
            ->where('id_product', $id)
            ->get()->first();
        return $tech;
    }
}
