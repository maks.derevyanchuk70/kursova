<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Technics;

class TechController extends Controller
{
    public function index(Request $request){
        $product_type = $request->input('product_type', null);

        $model_tech = new Technics();


        $tech = $model_tech->getTech($product_type);

        return view('app.techs.list', [
                'techs' => $tech,
                'product_type' => $product_type,]

        );
    }

    public function tech($id){
        $model_tech = new Technics();
        $tech = $model_tech->getTechByID($id);
        return view('app.techs.tech')->with('tech',  $tech);
    }

}
