<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Technics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminTechController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        return view('admin.products.list', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $price = $request->input('price');
        $product_type = $request->input('product_type');
        $products = new Product();
        $products->name = $name;
        $products->price = $price;
        $products->product_type = $product_type;
        $products->save();
        return Redirect::to('/admin/techs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::where('id_product', $id)->first();
        return view('admin.products.edit', [
            'products' => $products,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $products = Product::where('id_product', $id)->first();
        $products->name = $request->input('name');
        $products->price = $request->input('price');
        $products->product_type = $request->input('product_type');
        $products->save();
        return Redirect::to('/admin/techs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return Redirect::to('/admin/techs');
    }
}
