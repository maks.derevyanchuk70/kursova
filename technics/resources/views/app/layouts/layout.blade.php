<html>
<head>
    <title>Інформаційна система "Tech"</title>
</head>
<style>
    #table{
        border: black solid 3px;
        padding: 5px;
        margin: 5px;
        font-family: Impact;
        background-color: darkorange;
    }
    #cell{
        border: black solid 1px;
        padding: 20px;
        margin: 5px;
        font-family: Impact;
    }
    #text{
        font-family: Impact;
        font-size: 18px;
    }
    #header{
        font-family: Impact;
        font-size: 25px;
    }

</style>
<body>
@yield('page_title')
<br/><br/>
<div class="container">
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p id="header">Основна інформація</p>
        <p></p>
    </div>
    @yield('content')
</div>
<br/>
<br/>
</body>
@include('app.layouts.footer')
</html>
треба
