@extends('app.layouts.layout')


@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p id="header">Інформація про товар: {{ $tech->name }}</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="cell">Назва товару</th>
            <th id="cell">Ціна товару</th>
            <th id="cell">id товару</th>
            <th id="cell">Тип товару</th>
            <tr>
                <td id="cell">{{ $tech->name }}</td>
                <td id="cell">{{ $tech->price }}</td>
                <td id="cell">{{ $tech->id_product }}</td>
                <td id="cell">{{ $tech->product_type }}</td>
            </tr>
        </table>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <button onclick="window.location.href = href='/techs'" id="text">назад</button>
    </div>
@endsection
