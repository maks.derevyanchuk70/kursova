@extends('app.layouts.layout')

<?php
$model_tech = new App\Models\Technics();
$type = $model_tech->getTechType();

?>

@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <form method="get" action="/techs">
            <span id="text">тип товару :</span>
            <select name="product_type" id="text">
                <option value="0">всі</option>
                @foreach($type as $pr_type)
                    <option value="{{ $pr_type->product_type }}"
                        {{ ( $pr_type->product_type == $product_type ) ? 'selected' : '' }}>
                        {{ $pr_type->product_type }}
                    </option>
                @endforeach
            </select>
            <div class="container"  style="display: flex; justify-content: center; align-items: center; margin-top: 10px">
                <a id="text" href ='/admin/techs'>Редагування</a>
                <p style="margin: 5px"><input id="text" type="submit" value="виконати"/></p>
            </div>
            <div class="container"  style="display: flex; justify-content: center; align-items: center">
                <p id="text">список товару: </p>
            </div>
        </form>
    </div>

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <table id="table">

            <th id="cell">Назва товару</th>
            <th id="cell">Ціна товару</th>
            <th id="cell">Тип товару</th>

            @foreach ($techs as $tech)
                <tr>
                    <td id="cell">
                        <a href="/techs/{{ $tech->id_product }}">
                            {{ $tech->name}}
                        </a>
                    </td>
                    <td id="cell">{{ $tech->price }}</td>
                    <td id="cell">{{ $tech->product_type }}</td>
                </tr>
            @endforeach
        </table>
    </div>



@endsectio
    в кінці
