<html>
<head>
    <title>Інформаційна система "Tech"</title>
</head>
<style>
    #table{
        border: black solid 3px;
        padding: 5px;
        margin: 5px;
        font-family: Impact;
        background-color: yellow;
    }
    #cell{
        border: black solid 1px;
        padding: 20px;
        margin: 5px;
        font-family: Impact;
    }
    #text{
        font-family: Impact;
        font-size: 18px;
    }
    #header{
        font-family: Impact;
        font-size: 25px;
    }
</style>
<body>

<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b id="text">Товари</b>
    <ul>
        <li><a href="/admin/techs" id="text">список</a></li>
        <li><a href="/admin/techs/create" id="text">додати</a></li>
    </ul>
    <br/>
    <b id="text">Кабінет</b>
    <ul>
        <li><a href="/techs" id="text">фронтенд</a></li>
    </ul>
</div>
<div>
    @yield('content')
</div>
</body>
@include('app.layouts.footer')
</html>
