@extends('admin.layout')

<?php
$model_tech = new App\Models\Technics();
$type = $model_tech->getTechType();
?>

<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <h2 id="text_style2">Додавання товару</h2>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <form action="/admin/techs" method="POST">
            {{ csrf_field() }}
            <label>Назва товару: </label>
            <input type="text" name="name">
            <br/><br/>
            <label>Ціна товару: </label>
            <input type="text" name="price">
            <br/><br/>
            <label>Тип товару: </label>
            <input type="text" name="product_type">
            <br/><br/>
            <input type="submit" value="Додати">
        </form>
    </div>
@endsection
