@extends('admin.layout')

@section('content')

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <p id="header">Список товарів</p>
    </div>
    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="cell">Назва товару</th>
            <th id="cell">Ціна товару</th>
            <th id="cell">id товару</th>
            <th id="cell">Тип товару</th></th>
            <th id="cell">Опції</th>
            @foreach ($products as $tech)
                <tr>
                    <td id="cell">
                        <a href="/techs/{{ $tech->id_product }}">
                            {{ $tech->name}}
                        </a>
                    </td>
                    <td id="cell">{{ $tech->price }}</td>
                    <td id="cell">{{ $tech->id_product }}</td>
                    <td id="cell">{{ $tech->product_type }}</td>
                    <td id="cell">
                        <a href="/admin/techs/{{ $tech->id_product }}/edit"">редагувати</a>
                        <form style="margin:  5px;" action="/admin/techs/{{ $tech->id_product }}"method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button>Видалити</button>

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
